<section id="testimonials" class="inner-container">
	
	<div class="testimonial">
		<svg class="svg-icon flippy-icon"><use xlink:href="#icon-flippy" /></svg>
		<span class="h1">What Our Clients Have to Say</span>
		<p><?php echo ot_get_option('home_testimonials'); ?></p>

		<div class="pullquote">
			<p>Thanks for providing the extra ordinary vision and services to our very special children. Also thanks for mending this parent’s broken heart by giving us the help I craved!"</p>
			<span class="attribute">- Stacey Z.</span>
			
		</div>
		<a href="<?php echo esc_url(get_category_link( 13 )); ?> " class="btn pink">View More Testimonials</a>
	</div>

	<div class="video">
		<img src="<?php echo get_template_directory_uri(); ?>/library/img/video-thumb.jpg" class="thumb">
	</div>


</section>