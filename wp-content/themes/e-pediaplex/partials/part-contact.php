<section id="contact">
	<div class="inner-container">
		<div>
			<svg class="svg-icon flippy-icon"><use xlink:href="#icon-flippy" /></svg>
			<h2>Contact Us Today</h2>
			<p>We’d love to hear from you. Call us at <?php echo do_shortcode('[phone]'); ?> or complete our patient intake form. We’re to help guide your child’s development.</p>
			<a href="<?php echo get_permalink(33); ?>" class="btn"><svg class="svg-icon"><use xlink:href="#icon-form"></use></svg> Get Started</a>
		</div>
	</div>
	<svg class="svg-icon bg-icon"><use xlink:href="#icon-flippy" /></svg>
	<svg class="svg-icon bg-icon"><use xlink:href="#icon-flippy" /></svg>
</section>