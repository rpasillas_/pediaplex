<section id="candy-bar">
	<div class="inner-container">
		
		<blockquote>
			<p>Thanks for providing the extraordinary vision and services to our very special children. Also thanks for mending this parent’s broken heart by giving us the help I craved!</p>
			<cite>Stacey Z.</cite>
		</blockquote>						
		
		<div class="ribbon-cta">
			<a href="<?php echo get_permalink(); ?>" title="">Help Your Child Today</a>	
			<svg class="svg-ribbon"><use xlink:href="#left-banner-pink" /></svg>
			<svg class="svg-ribbon"><use xlink:href="#right-banner-pink" /></svg>
		</div>

	</div>
</section>