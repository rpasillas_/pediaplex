<?php 
	$args = array(
			'post_type'			=> 'slider_type',
			'posts_per_page'	=> 4,
			'orderby' 			=> 'sort_order',
			'order'				=> 'DESC',
		);
	$slider = new WP_Query( $args );
?>

<div class="slider-wrap" role="banner">
	<svg class="svg swoop"><use xlink:href="#slider-swoop" /></svg>

	<div id="slider" class="callbacks_container">
		<ul class="rslides">

		<?php while ( $slider -> have_posts() ) : $slider -> the_post(); ?>


			<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );  ?>

			<li style="background-image: url(<?php echo $large_image_url[0]; ?>);">
				<div class="inner-container">
					<div class="site-cta">
						<svg class="svg logo"><use xlink:href="#mainLogoWhiteText" /></svg>
						<?php echo do_shortcode('[phone]'); ?>
					</div>
					<div class="slide-text">
						<span class="h1"><?php the_title(); ?></span>
						<?php the_content(); ?>
						<a href="<?php echo get_permalink( get_post_meta($post->ID,'page_link', true) ); ?>" class="btn pink"><?php echo get_post_meta($post->ID,'page_cta', true); ?></a>
					</div>
				</div>
			</li>


		<?php endwhile; ?>	
		<?php wp_reset_postdata(); ?>

		</ul>
	</div>
	
	
</div>