<section id="insurance">
	<div class="inner-container">
		
		<div class="preamble">
			<svg class="svg-icon flippy-icon"><use xlink:href="#icon-flippy" /></svg>
			<h2>In-Network Insurance Accepted</h2>
			<p>Benefits are determined based upon insurance plan. Ready to start your child's journey?<br>Complete the <a href="<?php echo get_permalink(33); ?>">patient intake form</a>.</p>
		</div>
		<ul class="insurance-list">
			<li><img src="<?php echo get_template_directory_uri(); ?>/library/img/aetna.png" alt="AETNA"></li>
			<li><img src="<?php echo get_template_directory_uri(); ?>/library/img/blue-shield.png" alt="Blue Shield"></li>
			<li><img src="<?php echo get_template_directory_uri(); ?>/library/img/cigna.png" alt="Cigna"></li>
			<li><img src="<?php echo get_template_directory_uri(); ?>/library/img/scott-white-health.png" alt="Scott & White Health"></li>
			<li><img src="<?php echo get_template_directory_uri(); ?>/library/img/tricare.png" alt="Tricare"></li>
			<li><img src="<?php echo get_template_directory_uri(); ?>/library/img/united-healthcare.png" alt="United Healthcare"></li>
		</ul>

	</div>
</section>