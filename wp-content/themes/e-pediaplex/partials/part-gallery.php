<?php

	$args = array(
			'post_type'			=> 'gallery_type',
			'posts_per_page'	=> 8,
			'orderby' 			=> 'sort_order',
			'order'				=> 'DESC',
		);
	$gallery = new WP_Query( $args );
		
?>


<section id="gallery">
	<div class="inner-container">

		<div class="preamble">
			<svg class="svg-icon flippy-icon"><use xlink:href="#icon-flippy" /></svg>
			<span class="h2">Tour Our Center</span>
		</div>

		
		<div id="owl" class="gallery owl-carousel">
			<?php while ( $gallery -> have_posts() ) : $gallery -> the_post(); ?>
				<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); ?>
				<?php $small_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'gallery-thumb' ); ?>
				
				<div class="gallery-image-wrap">
					<div class="gallery-image">
						
						<a href="<?php echo esc_url($large_image_url[0]); ?>" class="fancybox" rel="HomeGallery">View Full Image</a>
						<svg class="svg-icon flippy-icon"><use xlink:href="#icon-flippy" /></svg>
						
						<svg class="svg-mask" width="264" height="264" viewBox="0 0 264 264" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" version="1.1">
						    <g>
						       <clipPath id="circle">
						          <circle fill="#E50278" cx="132" cy="132" r="132"/>
						       </clipPath>
						    </g> 
						     <image clip-path="url(#circle)" height="100%" width="100%" xlink:href="<?php echo $small_image_url[0]; ?>" />
						</svg>
										
					</div>
				</div>

			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</div>
		

	</div>
</section>