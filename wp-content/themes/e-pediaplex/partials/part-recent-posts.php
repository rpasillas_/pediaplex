<?php

	$args = array(
			'post_type'			=> 'post',
			'posts_per_page'	=> 3,
			'orderby' 			=> 'date',
			'order'				=> 'DESC',
		);
	$latest_posts_query = new WP_Query( $args );
		
?>

<section id="recent-posts">
	<div class="inner-container">
		<div class="preamble">
			<svg class="svg-icon flippy-icon"><use xlink:href="#icon-flippy" /></svg>
			<h2>Latest News</h2>
			<p>Read the latest news on child development services and resources for medical and emotional issues affecting children and their families.</p>
		</div>
		
		<section>
			<?php while ( $latest_posts_query -> have_posts() ) : $latest_posts_query -> the_post(); ?>
				
				<article class="article-summary">

						<header><h2><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title(); ?></a></h2></header>

						<time datetime="<?php echo get_the_time('Y-m-d'); ?>" itemprop="datePublished">
							<span class="month"><?php echo get_the_time('M'); ?></span>
							<span class="date"><?php echo get_the_time('d'); ?></span> 
							<span class="year"><?php echo get_the_time('Y'); ?></span>
						</time>

				</article>

			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</section>


		<a href="<?php echo esc_url(get_permalink( get_option('page_for_posts' ) )); ?>" class="btn pink">View All Articles</a>

	</div>
</section>