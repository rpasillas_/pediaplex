<?php

	$args = array(
			'post_type'			=> 'post',
			'posts_per_page'	=> 3,
			'orderby' 			=> 'date',
			'order'				=> 'DESC',
		);
	$latest_posts_query = new WP_Query( $args );
		
?>

<aside id="sidebar" class="sidebar">

	<header>Latest News</header>
		
	<section>
		<?php while ( $latest_posts_query -> have_posts() ) : $latest_posts_query -> the_post(); ?>
			
			<article class="article-summary">

					<header><h2><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2></header>

					<time datetime="<?php echo get_the_time('Y-m-d'); ?>" itemprop="datePublished">
						<span class="month"><?php echo get_the_time('M'); ?></span>
						<span class="date"><?php echo get_the_time('d'); ?></span> 
						<span class="year"><?php echo get_the_time('Y'); ?></span>
					</time>

			</article>

		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	</section>


	<a class="btn pink" href="<?php echo esc_url(get_permalink( get_option('page_for_posts' ) )); ?>">View All Articles</a>


</aside>