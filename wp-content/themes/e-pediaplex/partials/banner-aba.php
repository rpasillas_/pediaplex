 <div id="aba-banner">


		<div class="left">
			<div class="svg-wrap">
				<svg class="svg"><use xlink:href="#mainLogoWhiteText" /></svg>
			</div>

			<p>Offering individualized treatment plans for teaching vital behavioral skills to children with autism. Call now to learn about ABA therapy!</p>

			<span class="h3">Schedule a Tour</span>
			<?php echo do_shortcode('[phone]'); ?>	
		</div>			
		
		<div class="image-ribbon">
			<img src="<?php echo get_template_directory_uri(); ?>/library/img/children-playing-with-cups.png" alt="Children Playing With">

			<div class="ribbon-cta">
				<a href="<?php echo get_permalink(7); ?>">ABA Therapy</a>	
				<svg class="svg-ribbon"><use xlink:href="#left-banner-pink" /></svg>
				<svg class="svg-ribbon"><use xlink:href="#right-banner-pink" /></svg>
			</div>
		</div>



</div>