<?php

	$args = array(
			'post_type'			=> 'page',
			'posts_per_page'	=> 3,
			'orderby' 			=> 'date',
			'order'				=> 'DESC',
			'meta_value'		=> 'featured'
		);
	$featured_services = new WP_Query( $args );
		
?>







<section id="featured-services">
	<div class="inner-container">


		<div class="preamble">
			<svg class="svg-icon flippy-icon"><use xlink:href="#icon-flippy" /></svg>
			<span class="h2">Featured Therapy Services</span>
		</div>


		<div class="services">



			<?php while ( $featured_services -> have_posts() ) : $featured_services -> the_post(); ?>

				<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'gallery-thumb' ); ?>

				<div class="featured-service">
					<div class="service-thumb">
						<svg class="svg-mask" width="264" height="264" viewBox="0 0 264 264" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" version="1.1">
						    <g>
						       <clipPath id="circle">
						          <circle fill="#E50278" cx="132" cy="132" r="132"/>
						       </clipPath>
						    </g> 

						    <image clip-path="url(#circle)" height="100%" width="100%" xlink:href=" <?php echo $large_image_url[0]; ?>" />
						</svg>


						<div class="service-ribbon">
							<a href="<?php the_permalink(); ?>"><?php echo get_post_meta($post->ID, 'home_featured_title_meta_key', true); ?></a>
							<svg class="svg-ribbon"><use xlink:href="#left-banner-pink" /></svg>
							<svg class="svg-ribbon"><use xlink:href="#right-banner-pink" /></svg>
						</div>

					</div>
					<div class="service-description">
						<?php  echo get_post_meta($post->ID, 'home_featured_text_meta_key', true); ?>
					</div>

				</div>


			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>

				

		</div>



		<a class="btn pink" href="<?php echo esc_url(get_permalink(10)); ?>">Other Therapy Services</a>


	</div>
</section>