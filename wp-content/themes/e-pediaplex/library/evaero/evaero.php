<?php
/* 
 * EVAERO ITEMS
 *
 */




/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/


//----- Include Option Tree
        add_filter( 'ot_show_pages', '__return_false' );
        add_filter( 'ot_show_new_layout', '__return_false' );
        add_filter( 'ot_theme_mode', '__return_true' );
        include_once( 'option-tree/ot-loader.php' );
        include_once( 'inc/theme-options.php' );


//----- Custom Scripts Inclusion
        // Page Lineage
        include_once( 'inc/lineage.php' );

        // Siblings Widget
        include_once( 'inc/siblings.php' );

        //Walker
        include_once( 'inc/nav_walker_menu.php' );
        include_once( 'inc/select_walker_page.php' );



//----- PPC SETTINGS
        add_action('wp_head', 'ppc_options');
        function ppc_options(){
            $ppc = ot_get_option('ppc_google_analytics') . "\n";
            $ppc .= ot_get_option('ppc_google_remarketing') . "\n";
            $ppc .= ot_get_option('ppc_infinity') . "\n";
            $ppc .= ot_get_option('ppc_snapengage') . "\n";

            echo $ppc;
        }




        
//----- DEREGISTER CONTACT FORM 7 STYLES
        // http://voodoopress.com/configuring-contact-form-7-to-work-perfectly-with-a-twentyten-child-theme/
        add_action( 'wp_print_styles', 'voodoo_deregister_styles', 100 );
        function voodoo_deregister_styles() {
            wp_deregister_style( 'contact-form-7' );
        }




//----- Clean up the_excerpt()
        add_filter('excerpt_length', 'ge_excerpt_length');
        function ge_excerpt_length($length) {
          return 27;
        }




//----- Enable Shortcodes
        add_filter('the_content', 'do_shortcode');
        add_filter('post_content', 'do_shortcode');
        add_filter('widget_text', 'do_shortcode');
        include('inc/shortcodes.php');




//----- ANY CUSTOM POST TYPES
        require_once('custom-post-types/gallery-post-type.php');
        require_once('custom-post-types/slider-post-type.php');




//----- Register Additional Post Thumbnails
        if (class_exists('MultiPostThumbnails')) {
            new MultiPostThumbnails(
                array(
                    'label' => 'Blogroll Image',
                    'id' => 'blogroll-image',
                    'post_type' => 'post'
                )
            );
        }

        add_image_size( 'post-thumb-roll', 360, 260, 1 );
        add_image_size( 'gallery-thumb', 280, 280, 1 );

        function add_my_image_size_labels ($sizes) {
            $custom_sizes = array(
                'post-thumb-roll' => __('Blogroll Size'),
            );
            return array_merge( $sizes, $custom_sizes );
        }
        add_filter('image_size_names_choose', 'add_my_image_size_labels');

        function img_blogroll() {
            if (class_exists('MultiPostThumbnails')) :
                MultiPostThumbnails::the_post_thumbnail(
                    get_post_type(),
                    'blogroll-image'
                );
            endif;
        }


//----- Breadcrumb Function Formatting
        function breadcrumbs() {
            if ( function_exists('yoast_breadcrumb') ) {
                yoast_breadcrumb('<span class="breadcrumbs">','</span>');
            }
        }


//----- Print Article Title & Subtitle
        function title_and_subtitle() {
            $title = get_the_title();
            $subtitle = get_field('subtitle', false, false);
            if ($subtitle != "") {
                echo '<h1 itemprop="name" class="pre-sub">' . $title . '</h1><span class="subtitle">' . $subtitle . '</span>';
            } else {
                echo '<h1 itemprop="name">' . $title . '</h1>';
            }
        }


//----- Get Thumbnail URL
        function thumb_url() {
            $thumb_id = get_post_thumbnail_id();
            $the_thumb_url = wp_get_attachment_image_src($thumb_id,'full', true);
            if ( $thumb_id != '' ) {
                return $the_thumb_url[0];
            }
        }
        function post_thumb_url() {
            $thumb_id = get_post_thumbnail_id();
            $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
            echo $thumb_url[0];
        }


//----- Get Thumbnail Alt Text
        function thumb_alt() {
            $thumb_id = get_post_thumbnail_id(get_the_ID());
            $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
            if(count($alt)) echo $alt;
        }


//----- Copyright Year
        function copyright() {
            (date('Y') == "2014" ? $year = "2015" : $year = "2014 &ndash; " . date('Y')); echo $year;
         }


//----- Move Yoast Meta Box to Bottom
        add_filter( 'wpseo_metabox_prio', function() { return 'low';});


//----- Enable comment form & links
        add_action( 'comment_form_before', 'theme_enqueue_comment_reply_script' );
        function theme_enqueue_comment_reply_script() {
            if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
        }

        function theme_custom_pings( $comment ) {
            $GLOBALS['comment'] = $comment;
            ?>
                <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
            <?php
        }







//----- Social Media Icons
    function social_nav() {
        $data = '<div class="header-social-links"><span class="head">Follow Us</span> <span class="sub-trigger">+</span><ul class="sub-menu">';
        if ( ot_get_option('facebook') ) {
            $data .= '<li><a href="' . ot_get_option('facebook') . '" target="_blank" class="social-profile"><svg class="svg"><use xlink:href="#icon-facebook" /></svg></a></li>' . "\n";
        }
        if ( ot_get_option('twitter') ) {
            $data .= '<li><a href="' . ot_get_option('twitter') . '" target="_blank" class="social-profile"><svg class="svg"><use xlink:href="#icon-twitter" /></svg></a></li>' . "\n";
        }
        if ( ot_get_option('linkedin') ) {
            $data .= '<li><a href="' . ot_get_option('linkedin') . '" target="_blank" class="social-profile"><svg class="svg"><use xlink:href="#icon-linkedin" /></svg></a></li>' . "\n";
        }
        if ( ot_get_option('google') ) {
            $data .= '<li><a href="' . ot_get_option('google') . '" target="_blank" class="social-profile"><svg class="svg"><use xlink:href="#icon-google" /></svg></a></li>' . "\n";
        }
        if ( ot_get_option('youtube') ) {
            $data .= '<li><a href="' . ot_get_option('youtube') . '" target="_blank" class="social-profile"><svg class="svg"><use xlink:href="#icon-youtube" /></svg></a></li>' . "\n";
        }
        if ( ot_get_option('instagram') ) {
            $data .= '<li><a href="' . ot_get_option('instagram') . '" target="_blank" class="social-profile"><svg class="svg"><use xlink:href="#icon-instagram" /></svg></a></li>' . "\n";
        }
        if ( ot_get_option('pinterest') ) {
            $data .= '<li><a href="' . ot_get_option('pinterest') . '" target="_blank" class="social-profile"><svg class="svg"><use xlink:href="#icon-pinterest" /></svg></a></li>' . "\n";
        }

        $data .= '</ul></div>';   

        return $data;
    }



//----- Social Media Icons:: ALT
    function social_nav_alt() {
        $data = '<ul class="footer-social-links">';
        if ( ot_get_option('facebook') ) {
            $data .= '<li><a href="' . ot_get_option('facebook') . '" target="_blank" class="social-profile"><svg class="svg"><use xlink:href="#icon-facebook-alt" /></svg></a></li>' . "\n";
        }
        if ( ot_get_option('twitter') ) {
            $data .= '<li><a href="' . ot_get_option('twitter') . '" target="_blank" class="social-profile"><svg class="svg"><use xlink:href="#icon-twitter-alt" /></svg></a></li>' . "\n";
        }
        if ( ot_get_option('linkedin') ) {
            $data .= '<li><a href="' . ot_get_option('linkedin') . '" target="_blank" class="social-profile"><svg class="svg"><use xlink:href="#icon-linkedin-alt" /></svg></a></li>' . "\n";
        }
        if ( ot_get_option('google') ) {
            $data .= '<li><a href="' . ot_get_option('google') . '" target="_blank" class="social-profile"><svg class="svg"><use xlink:href="#icon-google-alt" /></svg></a></li>' . "\n";
        }
        if ( ot_get_option('youtube') ) {
            $data .= '<li><a href="' . ot_get_option('youtube') . '" target="_blank" class="social-profile"><svg class="svg"><use xlink:href="#icon-youtube-alt" /></svg></a></li>' . "\n";
        }
        if ( ot_get_option('instagram') ) {
            $data .= '<li><a href="' . ot_get_option('instagram') . '" target="_blank" class="social-profile"><svg class="svg"><use xlink:href="#icon-instagram-alt" /></svg></a></li>' . "\n";
        }
        if ( ot_get_option('pinterest') ) {
            $data .= '<li><a href="' . ot_get_option('pinterest') . '" target="_blank" class="social-profile"><svg class="svg"><use xlink:href="#icon-pinterest-alt" /></svg></a></li>' . "\n";
        }

        $data .= '</ul>';   

        return $data;
    }










//----- Meta Checkbox for Pages, to display as a featured page on the home page
    function featured_service_meta_box(){
        add_meta_box( 'page_featured', 'Featured Service', 'page_featured_callback', 'page', 'side' );        
    }

    add_action( 'add_meta_boxes', 'featured_service_meta_box' );


    function page_featured_callback($post){
        wp_nonce_field( 'featured_service_metabox', 'featured_service_nonce' );

        $featured_checkbox = get_post_meta( $post->ID, 'home_featured_checkbox_meta_key', true );
        $featured_title = get_post_meta( $post->ID, 'home_featured_title_meta_key', true );
        $featured_text = get_post_meta( $post->ID, 'home_featured_text_meta_key', true );

        $is_checked = (  $featured_checkbox == 'featured') ? 'checked="checked"' : '';


        echo '<label for="home_featured_checkbox"><strong>Display as Featured Service on Home Page?</strong></label><br>';
        echo '<input type="checkbox" id="home_featured_checkbox" name="home_featured_checkbox" value="featured" ' . $is_checked . '/><br><br>';
        echo '<label><strong>Small Title</strong></label><br>';
        echo '<input type="text" id="home_featured_title" name="home_featured_title" value="' . $featured_title . '" /><br><br>';
        echo '<label><strong>Description</strong></label><br>';
        echo '<textarea id="home_featured_text" name="home_featured_text" cols="30" rows="20">' . $featured_text . '</textarea>';
    }




    function page_featured_save( $post_id ) {
        
        if ( ! isset( $_POST['featured_service_nonce'] ) ) { return; }        
        if ( ! wp_verify_nonce( $_POST['featured_service_nonce'], 'featured_service_metabox' ) ) { return; }        
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }
        
        if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

            if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }

        } else {

            if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
        }


        /* OK, it's safe for us to save the data now. */
        
        // Make sure that it is set.
        if ( ! isset( $_POST['home_featured_checkbox'] ) ) {
            global $wpbd;

            //it wasn't checked, emtpy out the value
            //or we will have lots of empty keys to deal with
            delete_post_meta( $post_id, 'home_featured_checkbox_meta_key');
            delete_post_meta( $post_id, 'home_featured_text_meta_key');
            delete_post_meta( $post_id, 'home_featured_title_meta_key');
            return;
        }

        // Sanitize user input.
        $featured_checkbox = sanitize_text_field( $_POST['home_featured_checkbox'] );
        $featured_title = sanitize_text_field( $_POST['home_featured_title'] );
        $featured_text = sanitize_text_field( $_POST['home_featured_text'] );


        // Update the meta field in the database.
        update_post_meta( $post_id, 'home_featured_checkbox_meta_key', $featured_checkbox );
        update_post_meta( $post_id, 'home_featured_title_meta_key', $featured_title );
        update_post_meta( $post_id, 'home_featured_text_meta_key', $featured_text );

    }
    add_action( 'save_post', 'page_featured_save' );






//----- Replace blank option on Patient Intake Form
function my_wpcf7_form_elements($html) {
    if( is_page(33) ){
        $text = 'State';
        $html = str_replace('<option value="">---</option>', '<option value="">' . $text . '</option>', $html);   
    }
    return $html;
}
add_filter('wpcf7_form_elements', 'my_wpcf7_form_elements');


//----- Zen Desk Widget for Admins
    if ( function_exists( 'the_zendesk_webwidget' ) ) {
        $user = wp_get_current_user();
        if( user_can($user, 'administrator') ) add_action('wp_footer', 'the_zendesk_webwidget');    
    }