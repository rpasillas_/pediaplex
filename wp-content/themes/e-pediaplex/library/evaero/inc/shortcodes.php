<?php
//----- Panel
		function sc_panel($atts, $content = null) {
			extract( shortcode_atts(
				array(
					'title' => 'panel',
				),
				$atts ) );
			return '<div class="panel panel-default">
						<div class="panel-heading">' . esc_attr($title) . '</div>
						<div class="panel-body">
							<p>' . $content . '</p>
						</div>
					</div>';
		}
		add_shortcode('panel', 'sc_panel');





//----- Well
		function sc_well($atts, $content = null) {
			return '<div class="well">' . $content . '</div>';
		}
		add_shortcode('well', 'sc_well');








//----- Blockquote with Author
		function sc_blockquote($atts, $content = null) {
			extract(  shortcode_atts(
				array(
					'author' => 'blockquote',
				), $atts ) );

			return '<blockquote><p>' . $content . '</p><span class="author">' . esc_attr($author) . '</span></blockquote>';
		}
		add_shortcode('blockquote', 'sc_blockquote');









//----- Pullquote
		function sc_pullquote($atts, $content = null) {
			return '<span class="pullquote right">' . $content . '</span>';
		}
		add_shortcode('pullquote', 'sc_pullquote');
		add_shortcode('pullquote_right', 'sc_pullquote');










//----- Pullquote: Left
		function sc_pullquote_left($atts, $content = null) {
			return '<span class="pullquote left">' . $content . '</span>';
		}
		add_shortcode('pullquote_left', 'sc_pullquote_left');








//----- Double-Column List
		function sc_twocol($atts, $content = null) {
			return '<div class="double clear">' . $content . '</div>';
		}
		add_shortcode('twocol', 'sc_twocol');









//----- Button
		function sc_button($atts, $content = null) {
			extract( shortcode_atts(
				array(
					'id' => 'button',
				),
				$atts ) );
			return '<a href="<?php echo get_page_link( \'' . esc_attr($id) . '\' ); ?>">' . $content . '</a>';
		}
		add_shortcode('button', 'sc_button');












//----- CTA: Giant
		function sc_cta_giant($atts, $content = null) {
			return '<span class="cta giant">' . $content . '<br>' . do_shortcode('[phone]') . '</span>';
		}
		add_shortcode('cta_giant', 'sc_cta_giant');











//----- CTA: Large
		function sc_cta_large($atts, $content = null) {
			return '<span class="cta large">' . $content . '<br>' . do_shortcode('[phone]') . '</span>';
		}
		add_shortcode('cta_large', 'sc_cta_large');










//----- CTA: Regular
		function sc_cta($atts, $content = null) {
			return '<span class="cta">' . $content  . '<br>' . do_shortcode('[phone]') . '</span>';
		}
		add_shortcode('cta', 'sc_cta');








//----- CTA: Small
		function sc_cta_small($atts, $content = null) {
			return '<span class="cta small">' . $content . '<br>' . do_shortcode('[phone]') . '</span>';
		}
		add_shortcode('cta_small', 'sc_cta_small');











//----- CTA: Mini
		function sc_cta_mini($atts, $content = null) {
			return '<span class="cta mini">' . $content . '<br>' . do_shortcode('[phone]') . '</span>';
		}
		add_shortcode('cta_mini', 'sc_cta_mini');











//----- CTA: Call Now
		function sc_cta_call_now($atts, $content = null) {
			return '<div class="call-now">' . $content . ot_get_option( 'cta_bottom' ) . '</div>';
		}
		add_shortcode('call-now', 'sc_cta_call_now');











//----- CTA: Bottom
		function sc_cta_bottom($atts, $content = null) {
			return '<div class="cta-bottom"><svg class="svg-icon"><use xlink:href="#flippy-banner" /></svg>' . ot_get_option( 'cta_bottom' ) . ' ' . do_shortcode('[phone]') . '</div>';
		}
		add_shortcode('bottom', 'sc_cta_bottom');









//----- Other
		function sc_site_url($atts, $content = null) {
			return 'http://';
		}
		add_shortcode('site-url', 'sc_site_url');


		function sc_clear($atts, $content = null) {
			return '<span class="clear">' . $content . '</span>';
		}
		add_shortcode('clear', 'sc_clear');







//----- Candybar:: 
		//http://stackoverflow.com/questions/12325348/calling-wordpress-get-template-part-from-insde-a-shortcode-function-renders-temp
		function candybar($atts){	
			$data = '';
			//ob_start();	
			//get_template_part('partials/part', 'candybar');
			//$data = ob_get_contents();
			//ob_end_clean();
			return $data;
		}
		add_shortcode('cta_banner', 'candybar');




//----- Candybar:: 
		function aba_candybar($atts){	
			$data = '';
			ob_start();	
			get_template_part('partials/banner', 'aba');
			$data = ob_get_contents();
			ob_end_clean();
			return $data;
		}
		add_shortcode('aba_banner', 'aba_candybar');		







//----- Theme Options
		function sc_contact($atts, $content = null){
			$data = '<address class="address"><svg class="svg"><use xlink:href="#house" /></svg><span class="text">' . ot_get_option( 'address' ) . '</span></address>';
			$data .= '<div class="email"><svg class="svg"><use xlink:href="#airplane" /></svg><span class="text">' . do_shortcode('[email]') . '</span></div>';
			$data .= '<div class="phone"><svg class="svg"><use xlink:href="#phone" /></svg><span class="text">' . do_shortcode( '[phone]') . '</span></div>';
			return $data;
		}
		add_shortcode('contact_info', 'sc_contact');





//----- Address
		function sc_address($atts, $content = null) {
			return ot_get_option( 'address' );
		}
		add_shortcode('address', 'sc_address');




//----- Address: single line variation
		function sc_address_one_line($atts, $content = null) {
			$data = preg_replace( '/<br>/', ' | ', ot_get_option( 'address' ) );
			return $data;
		}
		add_shortcode('address_single', 'sc_address_one_line');



//----- Email
		function sc_email($atts, $content = null) {
			return '<a href="mailto:' . ot_get_option( 'email' ) . '">' . ot_get_option( 'email' ) . '</a>';
		}
		add_shortcode('email', 'sc_email');





//----- Fax
		function sc_fax($atts, $content = null) {
			return ot_get_option( 'fax' );
		}
		add_shortcode('fax', 'sc_fax');






//----- Phone
		function sc_phone($atts, $content = null) {
			$span  = ot_get_option( 'ppc_infinity_span' );
			$num   = ot_get_option( 'phone' );
			$rid   = array('-','(',')',' ');
			$clean = (str_replace($rid,'',$num));
			if ($span != '') {
				return '<span class="desktop phone ' . $span . ' clickable"><span class="num">' . $num . '</span></span>';
			} else if (strpos($num,'+') !== 'false') {
				return '<a class="desktop phone" href="tel:' . $clean . '"><span class="num">' . $num . '</span></a>';
			} else {
				return '<a class="desktop phone" href="tel:+' . $clean . '"><span class="num">' . $num . '</span></a>';
			}
		}
		add_shortcode('phone', 'sc_phone');




//----- Phone [mobile: shows phone icon]
		function sc_phone_mobile($atts, $content = null) {
			$span  = ot_get_option( 'ppc_infinity_span' );
			$num   = ot_get_option( 'phone' );
			$rid   = array('-','(',')',' ');
			$clean = (str_replace($rid,'',$num));
			if ($span != '') {
				return '<span class="mobile ' . $span . ' clickable"><span class="num"></span></span>';
			} else if (strpos($num,'+') !== 'false') {
				return '<a class="mobile" href="tel:' . $clean . '"><span class="num"></span></a>';
			} else {
				return '<a class="mobile" href="tel:+' . $clean . '"><span class="num"></span></a>';
			}
		}
		add_shortcode('phone_mobile', 'sc_phone_mobile');




//----- Phone [Alternate]
		function sc_phone_alt($atts, $content = null) {
			$span  = ot_get_option( 'ppc_infinity_span' );
			$num   = ot_get_option( 'phone_us' );
			$rid   = array('-','(',')',' ');
			$clean = (str_replace($rid,'',$num));
			if ($span != '') {
				return '<span class="' . $span . ' clickable" id="call-btn-mbl">' . $content . '</span>';
			} else if (strpos($num,'+') !== 'false') {
				return '<a href="tel:' . $clean . '" id="call-btn-mbl">' . $content . '</a>';
			} else {
				return '<a href="tel:+' . $clean . '" id="call-btn-mbl">' . $content . '</a>';
			}
		}
		add_shortcode('phone-alt', 'sc_phone_alt');




//----- Phone [No Admit]
		function sc_phone_noadmit($atts, $content = null) {
			$span  = ot_get_option( 'ppc_infinity_span' );
			$num   = ot_get_option( 'phone_noadmit' );
			$rid   = array('-','(',')',' ');
			$clean = (str_replace($rid,'',$num));
			if ($span != '') {
				return '<span class="' . $span . ' clickable">' . $num . '</span>';
			} else if (strpos($num,'+') !== 'false') {
				return '<a href="tel:' . $clean . '">' . $num . '</a>';
			} else {
				return '<a href="tel:+' . $clean . '">' . $num . '</a>';
			}
		}
		add_shortcode('phone_noadmit', 'sc_phone_noadmit');				






//----- Patient Intake Form CTA
		function sc_intake_link($atts, $content = null){
			$page = get_permalink(33);
			$data = '';
			$data .= '<a href="' . $page . '" title="Complete the patient intake form"><svg class="svg-icon"><use xlink:href="#icon-form" /></svg></a>';
			$data .= '<p class="intake-link">Ready to start your child\'s journey? Complete the patient intake form.';
			$data .= '</p>';
			return $data;
		}
		add_shortcode('intake-link', 'sc_intake_link');





//----- Patient Intake Form CTA		
		function sc_flippy($atts, $content = null){
			return '<svg class="svg-icon flippy-icon"><use xlink:href="#icon-flippy" /></svg>';
		}
		add_shortcode('flippy', 'sc_flippy');