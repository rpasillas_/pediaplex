<?php
// Creating the widget 
class wpb_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			// Base ID of your widget
			'siblings_nav', 

			// Widget name will appear in UI
			__('Siblings Widget', 'wpb_widget_domain'), 

			// Widget description
			array( 'description' => __( 'A list of the current page\'s sibling pages.', 'wpb_widget_domain' ), ) 
		);
	}




	public function sibling_nav(){
		//this comes from the includes in the functions file
		$id = get_top_parent_page_id();
        
        $data = wp_list_pages(array(
                    'child_of'      => $id,
                    'sort_column'   => 'post_title',
                    'sort_order'    => 'ASC',
                    'title_li'      => '',
                    'depth'         => 1,
                    'echo'          => 0
                ));

       return ($data == '') ?  false :  $data;

    } 

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		

		if ( $this->sibling_nav() !== false ) {

			// before and after widget arguments are defined by themes
			echo $args['before_widget'];
			if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

			// This is where you run the code and display the output
			echo '<div class="section-nav"><nav><ul>';

			echo $this->sibling_nav();
			echo '</ul></nav></div>';
			echo $args['after_widget'];
		} 
		
	}
			
	// Widget Backend 
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = 'In This Section';
		}
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
} // Class wpb_widget ends here



// Register and load the widget
function wpb_load_widget() {
	register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );