<?php
add_action( 'admin_init', 'custom_theme_options', 1 );
function custom_theme_options() {
	$saved_settings = get_option( 'option_tree_settings', array() );
	$custom_settings = array(
		'contextual_help' => array(
			'sidebar' => ''
		),
		'sections' => array(
			array(
				'id' => 'content_settings',
				'title' => 'Content Settings'
			),
			array(
				'id' => 'ppc_settings',
				'title' => 'PPC Settings'
			),
		),
		'settings' => array(

		
		
		
		
		
		
		
		
		
		
		/*===========================================================================*\
			CONTACT INFORMATION
		\*===========================================================================*/
			
			// Tab
			array(
				'section' => 'content_settings',
				'id' => 'tab_contact',
				'type' => 'tab',
				'label' => 'Contact Information',
			),

				// Phone Number
				array(
					'section' => 'content_settings',
					'id' => 'phone',
					'type' => 'text',
					'label' => 'Phone Number',
					'desc' => 'Enter the client\'s phone number.<br/><br/><strong>Shortcode:</strong><br/><span style="color:#0096d1">[phone]</span>',
				),

				// Email Address
				array(
					'section' => 'content_settings',
					'id' => 'email',
					'type' => 'text',
					'label' => 'Email Address',
					'desc' => 'Enter the main email address for the site.<br/><br/><strong>Shortcode:</strong><br/><span style="color:#0096d1">[email]</span>',
				),

				// Street Address
				array(
					'section' => 'content_settings',
					'id' => 'address',
					'label' => 'Street Address',
					'desc' => 'Enter the client\'s physical location. Separate lines with the break tag.<br/><br/><strong>Shortcode:</strong><br/><span style="color:#0096d1">[address]</span>',
					'type' => 'textarea-simple',
					'rows' => '5',
				),

				// Facebook
				array(
					'section' => 'content_settings',
					'id' => 'facebook',
					'label' => 'Facebook',
					'desc' => 'Enter the client\'s Facebook URL',
					'type' => 'text',
				),

				// Twitter
				array(
					'section' => 'content_settings',
					'id' => 'twitter',
					'label' => 'Twitter',
					'desc' => 'Enter the client\'s Twitter URL',
					'type' => 'text',
				),

				// Google+
				array(
					'section' => 'content_settings',
					'id' => 'google',
					'label' => 'Google+',
					'desc' => 'Enter the client\'s Google+ URL',
					'type' => 'text',
				),

				// LinkedIn
				array(
					'section' => 'content_settings',
					'id' => 'linkedin',
					'label' => 'LinkedIn',
					'desc' => 'Enter the client\'s LinkedIn URL',
					'type' => 'text',
				),

				// Pinterest
				array(
					'section' => 'content_settings',
					'id' => 'pinterest',
					'label' => 'Pinterest',
					'desc' => 'Enter the client\'s Pinterest URL',
					'type' => 'text',
				),

				// Instagram
				array(
					'section' => 'content_settings',
					'id' => 'instagram',
					'label' => 'Instagram',
					'desc' => 'Enter the client\'s Instagram URL',
					'type' => 'text',
				),

				// YouTube
				array(
					'section' => 'content_settings',
					'id' => 'youtube',
					'label' => 'YouTube',
					'desc' => 'Enter the client\'s YouTube URL',
					'type' => 'text',
				),
								
			
			
			
			
			
			
			
		/*===========================================================================*\
			SECTION BANNERS
		\*===========================================================================*/
		
			// Tab
			array(
				'section' => 'content_settings',
				'id' => 'tab_banners',
				'label' => 'Section Banners',
				'desc' => '',
				'type' => 'tab',
			),

				// ABA Therapy: Text
				array(
					'section' => 'content_settings',
					'id' => 'banner_title_7',
					'label' => 'Text: ABA Therapy',
					'desc' => 'Choose the banner text for the ABA Therapy section.',
					'type' => 'text',
				),

				// ABA Therapy: Banner Background
				array(
					'section' => 'content_settings',
					'id' => 'banner_bg_7',
					'label' => 'Banner Background: ABA Therapy',
					'desc' => 'Choose the banner background for the ABA Therapy section.',
					'type' => 'upload',
				),

				// Autism: Text
				array(
					'section' => 'content_settings',
					'id' => 'banner_title_943',
					'label' => 'Text: Autism',
					'desc' => 'Choose the banner text for the Autism section.',
					'type' => 'text',
				),

				// Autism: Banner Background
				array(
					'section' => 'content_settings',
					'id' => 'banner_bg_943',
					'label' => 'Banner Background: Autism',
					'desc' => 'Choose the banner background for the Autism section.',
					'type' => 'upload',
				),

				// Speech Therapy: Text
				array(
					'section' => 'content_settings',
					'id' => 'banner_title_8',
					'label' => 'Text: Speech Therapy',
					'desc' => 'Choose the banner text for the Speech Therapy section.',
					'type' => 'text',
				),

				// Speech Therapy: Banner Background
				array(
					'section' => 'content_settings',
					'id' => 'banner_bg_8',
					'label' => 'Banner Background: Speech Therapy',
					'desc' => 'Choose the banner background for the Speech Therapy section.',
					'type' => 'upload',
				),

				// Occupational Therapy: Text
				array(
					'section' => 'content_settings',
					'id' => 'banner_title_9',
					'label' => 'Text: Occupational Therapy',
					'desc' => 'Choose the banner text for the Occupational Therapy section.',
					'type' => 'text',
				),

				// Occupational Therapy: Banner Background
				array(
					'section' => 'content_settings',
					'id' => 'banner_bg_9',
					'label' => 'Banner Background: Occupational Therapy',
					'desc' => 'Choose the banner background for the Occupational Therapy section.',
					'type' => 'upload',
				),

				// Other Therapy Services: Text
				array(
					'section' => 'content_settings',
					'id' => 'banner_title_10',
					'label' => 'Text: Other Therapy Services',
					'desc' => 'Choose the banner text for the Other Therapy Services section.',
					'type' => 'text',
				),

				// Other Therapy Services: Banner Background
				array(
					'section' => 'content_settings',
					'id' => 'banner_bg_10',
					'label' => 'Banner Background: Other Therapy Services',
					'desc' => 'Choose the banner background for the Other Therapy Services section.',
					'type' => 'upload',
				),
				
				// Psychology Services: Text
				array(
					'section' => 'content_settings',
					'id' => 'banner_title_11',
					'label' => 'Text: Psychology Services',
					'desc' => 'Choose the banner text for the Psychology Services section.',
					'type' => 'text',
				),

				// Psychology Services: Banner Background
				array(
					'section' => 'content_settings',
					'id' => 'banner_bg_11',
					'label' => 'Banner Background: Psychology Services',
					'desc' => 'Choose the banner background for the Psychology Services section.',
					'type' => 'upload',
				),


				//Our Philosophy: Text
				array(
					'section' => 'content_settings',
					'id' => 'banner_title_12',
					'label' => 'Text: Our Philosophy',
					'desc' => 'Choose the banner text for the Our Philosophy section.',
					'type' => 'text',
				),

				//Our Philosophy: Banner Background
				array(
					'section' => 'content_settings',
					'id' => 'banner_bg_12',
					'label' => 'Banner Background: Our Philosophy',
					'desc' => 'Choose the banner background for the Our Philosophy section.',
					'type' => 'upload',
				),


				//Contact: Text
				array(
					'section' => 'content_settings',
					'id' => 'banner_title_13',
					'label' => 'Text: Contact',
					'desc' => 'Choose the banner text for the Contact section.',
					'type' => 'text',
				),

				//Contact: Banner Background
				array(
					'section' => 'content_settings',
					'id' => 'banner_bg_13',
					'label' => 'Banner Background: Contact',
					'desc' => 'Choose the banner background for the Contact section.',
					'type' => 'upload',
				),


				//Blog: Text
				array(
					'section' => 'content_settings',
					'id' => 'banner_title_blog',
					'label' => 'Text: Blog',
					'desc' => 'Choose the banner text for the Blog section.',
					'type' => 'text',
				),

				//Blog: Banner Background
				array(
					'section' => 'content_settings',
					'id' => 'banner_bg_blog',
					'label' => 'Banner Background: Blog',
					'desc' => 'Choose the banner background for the Blog section.',
					'type' => 'upload',
				),

			
			
			
			
			
			
			
		
		
		/*===========================================================================*\
			CTAs
		\*===========================================================================*/
			
			// Tab
			array(
				'section' => 'content_settings',
				'id' => 'tab_ctas',
				'label' => 'CTAs',
				'desc' => '',
				'type' => 'tab',
			),

				// Bottom-of-Page CTA
				array(
					'section' => 'content_settings',
					'id' => 'cta_bottom',
					'label' => 'Bottom-of-Page CTA',
					'desc' => 'Text for the "Call Now" CTA. Do not enter phone; phone gets inserted automatically.<br/><br/><strong>Shortcode:</strong><br/><span style="color:#0096d1">[bottom]</span><span style="color:#757575">Your Message</span><span style="color:#0096d1">[/bottom]</span><br/><br/>',
					'type' => 'textarea',
				),
			
			
			
			
			
			
			
		
		
		/*===========================================================================*\
			SITE BLURBS
		\*===========================================================================*/
			
			// Tab
			array(
				'section' => 'content_settings',
				'id' => 'tab_blurbs',
				'label' => 'Site Blurbs',
				'desc' => '',
				'type' => 'tab',
			),

				// Bottom-of-Page CTA
				array(
					'section' => 'content_settings',
					'id' => 'home_testimonials',
					'label' => 'Home Testimonials Blurb',
					'desc' => 'Copy for the <em>What Our Clients Have to Say</em> section on the home page.',
					'type' => 'textarea-simple',
				),







		/*===========================================================================*\
			SHORTCODES
		\*===========================================================================*/
		
			// Tab
			array(
				'section' => 'content_settings',
				'id' => 'tab_shortcodes',
				'label' => 'Shortcodes',
				'desc' => 'You will see all additional shortcodes in this area.',
				'type' => 'tab',
			),
			
		





			
			
			
			
			
		/*===========================================================================*\
			PPC SETTINGS
		\*===========================================================================*/
		
			// Tab
			array(
				'section' => 'ppc_settings',
				'id' => 'tab_ppc_google_analytics',
				'label' => 'Google Analytics',
				'type' => 'tab',
			),

				// Google Analytics Code
				array(
					'section' => 'ppc_settings',
					'id' => 'ppc_google_analytics',
					'label' => 'Google Analytics Code',
					'desc' => 'This is the area to enter the client\'s Google Analytics code.',
					'type' => 'textarea-simple',
					'rows' => '10',
				),

			// Tab
			array(
				'section' => 'ppc_settings',
				'id' => 'tab_ppc_google_remarketing',
				'label' => 'Google Remarketing',
				'desc' => '',
				'type' => 'tab',
			),

				// Google Remarketing Code
				array(
					'section' => 'ppc_settings',
					'id' => 'ppc_google_remarketing',
					'label' => 'Google Remarketing Code',
					'desc' => 'This is the area to enter the client\'s Google Remarketing code.',
					'type' => 'textarea-simple',
					'rows' => '10',
				),

			// Tab
			array(
				'section' => 'ppc_settings',
				'id' => 'tab_ppc_infinity',
				'label' => 'Infinity',
				'type' => 'tab',
			),

				// Infinity Code
				array(
					'section' => 'ppc_settings',
					'id' => 'ppc_infinity',
					'label' => 'Infinity Code',
					'desc' => 'This is the area to enter the client\'s Infinity code.',
					'type' => 'textarea-simple',
					'rows' => '10',
				),

				// Infinity Span
				array(
					'section' => 'ppc_settings',
					'id' => 'ppc_infinity_span',
					'label' => 'Infinity Span Class',
					'desc' => 'This is the area to enter the client\'s Infinity span class for their phone number.',
					'type' => 'text',
				),

			// Tab
			array(
				'section' => 'ppc_settings',
				'id' => 'tab_snapengage',
				'label' => 'SnapEngage',
				'type' => 'tab',
			),

				// SnapEngage Code
				array(
					'section' => 'ppc_settings',
					'id' => 'ppc_snapengage',
					'label' => 'SnapEngage Code',
					'desc' => 'This is the area to enter the client\'s SnapEngage code.',
					'type' => 'textarea-simple',
					'rows' => '10',
				),


			



		)
	);

	/* allow settings to be filtered before saving */
	$custom_settings = apply_filters( 'option_tree_settings_args', $custom_settings );

	/* settings are not the same update the DB */
	if ( $saved_settings !== $custom_settings ) {
		update_option( 'option_tree_settings', $custom_settings );
	}
}