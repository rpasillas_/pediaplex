<?php

//http://badfunproductions.com/create-a-custom-walker-class-to-extend-wp_list_pages/
//https://codex.wordpress.org/Class_Reference/Walker

class Select_Walker_Page extends Walker {
   
   //gets passed as an argument when this class is instantiated 
    var $page_link;

    function __construct($page_link){
        $this->page_link = $page_link;
    }

    // Tell Walker where to inherit it's parent and id values
    var $db_fields = array(
        'parent' => 'menu_item_parent', 
        'id'     => 'db_id' 
    );

    /**
     * At the start of each element, output a <li> and <a> tag structure.
     * 
     * Note: Menu objects include url and title properties, so we will use those.
     */
    //&$output, $object, $depth = 0, $args = Array, $current_object_id = 0
    function start_el(&$output, $object, $depth = 0, $args = array(), $current_object_id=0 ) {
        
        if ( $depth )
            $indent = str_repeat("\t", $depth);
        else
            $indent = '';
 
        extract($args, EXTR_SKIP);

        $selected = ($this->page_link == $object->ID) ? 'selected': '';

        $output .= $indent . '<option value="' . $object->ID . '" ' . $selected . '>' . get_the_title($object->ID) . '</option>';

    }


}