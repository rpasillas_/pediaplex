<?php

//----- This script retrieves the ID of the top root ancestor of a page.

function is_tree($pid) { //$pid = The ID of the ancestor page
	global $post;
	$anc = get_post_ancestors( $post->ID );
	foreach($anc as $ancestor) {
		if(is_page() && $ancestor == $pid) {
			return true;
		}
	}
	if ( is_page() && (is_page($pid)) ) {
		return true;
	} else {
		return false;
	}
}

function get_top_parent_page_id() {
	global $post;
	$ancestors = $post->ancestors;
	if ($ancestors) {
		return end($ancestors);
	} else if( $post ) {
		return $post->ID;
	}else{
		return '';
	}
}

function e_lineage_root_parent_title() {
	$id = get_top_parent_page_id();

	if( is_author() ){
		echo wp_title();
	} else if ( is_post_type_archive('gallery_type') ) {
		echo 'Gallery';
	} else if (is_home() || is_single() || is_archive() || is_category()) {
		echo ot_get_option( 'banner_title_blog' );
	} else if (is_404()) {
		echo 'Page Not Found';
	} else if ($id) {
		echo ot_get_option( 'banner_title_' . $id );
	} else {
		wp_title();
	}
}

function e_lineage_root_parent_img() {
	$id = get_top_parent_page_id();
	if (is_home() || is_single() || is_archive()) {
		$image = ot_get_option( 'banner_bg_blog' );
		
		//if no image is uploaded, dont return a blank style declaration 
		// that over writes the default image defined in stylesheet
		if($image){
			echo ' style="background-image:url(\'' . $image . '\');"';
		}
	} else if ($id) {
		$image = ot_get_option( 'banner_bg_' . $id );

		//if no image is uploaded, dont return a blank style declaration 
		// that over writes the default image defined in stylesheet
		if( $image ){
			echo ' style="background-image:url(\'' . $image . '\');"';
		}
		
	}
}