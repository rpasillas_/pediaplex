<?php

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'bones_flush_rewrite_rules' );

// Flush your rewrite rules
function bones_flush_rewrite_rules() {
	flush_rewrite_rules();
}

// let's create the function for the custom type
function gallery_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'gallery_type', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => 'Facility Gallery', /* This is the Title of the Group */
			'singular_name' => 'Facility Image', /* This is the individual type */
			'all_items' => 'All Images', /* the all items menu item */
			'add_new' => 'Add New', /* The add new menu item */
			'add_new_item' => 'Add New Facility Image', /* Add New Display Title */
			'edit' => 'Edit', /* Edit Dialog */
			'edit_item' => 'Edit Facility Image', /* Edit Display Title */
			'new_item' => 'New Facility Image', /* New Display Title */
			'view_item' => 'View Facility Image', /* View Display Title */
			'search_items' => 'Search Facility Image', /* Search Custom Type Title */ 
			'not_found' =>  'Nothing found in the Database.', /* This displays if there are no entries yet */ 
			'not_found_in_trash' => 'Nothing found in Trash', /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => 'Facility Images', /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-format-gallery', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'gallery', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'gallery', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'thumbnail', 'custom-fields', 'page-attributes'),
			'show_in_rest'		=> true
		) /* end of options */
	); /* end of register post type */
	
}

	// adding the function to the Wordpress init
	add_action( 'init', 'gallery_post_type');
	
	

?>
