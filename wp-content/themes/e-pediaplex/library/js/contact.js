//https://developers.google.com/maps/documentation/javascript/examples/
//http://stackoverflow.com/questions/2330197/how-to-disable-mouse-scroll-wheel-scaling-with-google-maps-api


var map;
function initialize() {
  var mapOptions = {
    zoom: 17,
    center: new google.maps.LatLng(32.9392832, -97.11364479999999),
    disableDefaultUI: true,
    scrollwheel: false,
  };
  map = new google.maps.Map(document.getElementById('map'),
      mapOptions);
}

google.maps.event.addDomListener(window, 'load', initialize);