(function ($, root, undefined) {	
	$(function () {
		
		'use strict';
		
		//variables for reverting a mobile nav to desktop nav
		//windowWidth on line 37 is a global variable defined in footer.php
		
		var isVisible = false,
			formState = 'close';

		var formTop = '52px';
		var resizeTimer;



		/* START Mobile Nav
	    ============================================================================
	    */

		//make mobile menu open and close
		$('#mobile-menu-trigger').on('click', function(){			
			$('#nav').toggleClass('open');
			$(this).toggleClass('open');
		});


		mobile();



		//on window resize, return nav to desktop style 
		//if window is larger than mobile
	    function resizeFunction() {
	        mobile();
	        doOwlCarousel();
	    };
	    $(window).resize(function() {
	        clearTimeout(resizeTimer);
	        resizeTimer = setTimeout(resizeFunction, 250);
	    });



		function mobile(){
			if(windowWidth < mobileWidth){
				isVisible = true;
				$('#nav').removeClass('open');

			}else{	
				if(isVisible){									
					isVisible = false;
					$('#nav').removeClass('open');
				}

			}
		}



		//make mobile menu sub-nav open and close
		$('.sub-trigger').on('click', function(){
			var el = $(this);
			el.toggleClass('on');
			el.siblings('.sub-menu').toggleClass('on');
			el.closest('li').toggleClass('on');
		});




		/* START Back to Top Button
	    ============================================================================
	    */
	    $('#totop').click(function() {
	        $("html, body").animate({
	            scrollTop: 0
	        }, {duration: 600, specialEasing: 'easeOutExpo', queue: false});
	        return false;
	    });


	    /* FANCYBOX
	    ============================================================================
	    */
	    $(".fancybox").fancybox({
			maxWidth	: 800,
			maxHeight	: 600,
			fitToView	: true,
			width		: '70%',
			height		: '70%',
			autoSize	: false,
			closeClick	: false,
			openEffect	: 'elastic',
			closeEffect	: 'fade'
		});


	    //owl carousel
	    function doOwlCarousel(){
			if(windowWidth < 700 ){
				$('#owl').owlCarousel({
		    		items: 1,
		    		itemsMobile: [600, 1],
		    		autoPlay: true,
		    		pagination: false
		    	});
	    	}else{
	    		var owl = $('#owl').data('owlCarousel');
	    		if(owl){ owl.destroy(); }
	    	}
		}


	    $(document).ready(function(){
	    	doOwlCarousel();		    			  	
	    });



	    //SHARE BUTTONS HOVER FX
		$('.js-ssba-btn').hover(
			function(){
				$(this).toggleClass('open');
				$('div.ssba').toggleClass('open');
			},function(){
				var $el = $(this);
				window.setTimeout( function(){ $el.toggleClass('open'); }, 500);
				$('div.ssba').toggleClass('open');
			});



		$('.gallery-image').click(function(){
		});



		/* FORM POP-UP
	    ============================================================================
	    */
	    

	    if( Cookies.get('pediaplex-popup') !== 'true' ){
	    	setTimeout(function(){ formPopUp(); }, 10000);	    	
	    	
	    	$(document).ready(function(){

	    		$(window).on('scroll', function(){
	    			var scrollPosition = $(this).scrollTop();
	    			if( scrollPosition >= 1800){
	    				formPopUp();
	    				$(this).off('scroll');
	    			}
	    		});
	    	});
		   
	}


	var alreadyDisplayed;

	    function formPopUp(){

	    	if( alreadyDisplayed == null){
	    		$('#formTrigger').fancybox({
		    		wrapCSS		: 'abaPopUp',
		    		minWidth	: 280,
		    		minHeight	: 580,
		    		maxWidth	: 600,
		    		maxHeight	: 580,
		    		width 		: '60%',
		    		height 		: '100%',
		    		fitToView 	: false,
		    		autoSize 	: false,
		    		padding	: 0,
		    		margin		: [40,0,0,0],
		    		helpers:{
			    		overlay:{
			    			locked: true
			    		}
		    		},
		    		afterShow: function(){ 
		    			Cookies.set('pediaplex-popup', 'true', { expires: 7 });
		    			$(window).off('scroll');
		    			alreadyDisplayed = true;
		    		} 
		    	});

		    	$('#formTrigger').trigger('click');
	    	}
	    	
	    }

	    function formPopUpClose(){
	    	$.fancybox.close();
	    }

	    $(document).on('mailsent.wpcf7', function () {
	    	setTimeout(function(){ formPopUpClose(); }, 2000);	    
		});



		/* INTAKE FORM
	    ============================================================================
	    */

	    //SHOW DIVORCE/SEPARATED/NOT MARRIED BOXES
	    $('input:radio[name=pi-parents]').change(function(){
	    	var selected = $('input:radio[name=pi-parents]:checked').val(),
	    		fields = ['Divorced', 'Remarried', 'Never Married'],
	    		element= $('.wrap-otherparent');
	    	showMoreFields(fields, selected, element);
	    });

	    //SHOW DUAL-DIAGNOSIS TEXTFIELD
	    $('input:radio[name=pi-diagnosis]').change(function(){
	    	var selected = $('input:radio[name=pi-diagnosis]:checked').val(),
	    		fields = ['Yes'],
	    		element= $('.wrap-diagnosis-details');
	    	showMoreFields(fields, selected, element);
	    });

	    //SHOW PAST/PRESENT MEDICAL PROBLEMS TEXTFIELD
	    $('input:radio[name=pi-problems]').change(function(){
	    	var selected = $('input:radio[name=pi-problems]:checked').val(),
	    		fields = ['Yes'],
	    		element= $('.wrap-explanation');
	    	showMoreFields(fields, selected, element);
	    });

	    //SHOW AGGRESSIVE BEHAVIOR PROBLEMS TEXTFIELD
	    $('input:radio[name=pi-aggressive]').change(function(){
	    	var selected = $('input:radio[name=pi-aggressive]:checked').val(),
	    		fields = ['Yes'],
	    		element= $('.wrap-agressive-details');
	    	showMoreFields(fields, selected, element);
	    });

	    //SHOW/HIDE FIELDS
	    function showMoreFields(fields, selected, element){
	    	element.removeClass('on');
	    	$.each(fields, function(index, value){
	    		if( value == selected){
	    			element.addClass('on');
	    		}
	    	});
	    }

	    //INSERT ASTERISK INTO FIRST STATE SELECT
	    $('select[name="pi-g-state"] > option:first-child').text('State*');

	    //FATHER CHECKED
	    $('input[name="pi-f-checkbox[]"]').click(function(){
	    	clearFields('mother');
	    	var copyFields = copyGuardianFields('father');
	    	if( $('.father-section').hasClass('on')){
	    		$('.father-section').toggleClass('on');	    		
	    	}else{
	    		copyFields.done(function(){
	    			$('.father-section').toggleClass('on');
	    		});
	    	}		    	
	    	$('input[name="pi-f-checkbox-2[]"]').prop( "checked", false );
	    });

	    //MOTHER CHECKED
	    $('input[name="pi-m-checkbox[]"]').click(function(){	
	    	clearFields('father');
	    	var copyFields = copyGuardianFields('mother');
	    	if( $('.mother-section').hasClass('on')){
	    		$('.mother-section').toggleClass('on');
	    		return;
	    	}	    	
	    	copyFields.done(function(){	    		
	    		$('.mother-section').toggleClass('on');	    		
	    	});
	    });

	    //FATHER NOT REQUIRED
	    $('input[name="pi-f-checkbox-2[]"]').click(function(){	
	    	clearFields('father');
	    	$('.father-section').toggleClass('on');
	    });

	    //COPY FIELDS
	    //SET A DEFERRED OBJECT
	    function copyGuardianFields(parent){
	    	var deferred = $.Deferred();

	    	var name = $('input[name=pi-guardian]').val(),
	    		addr = $('input[name=pi-g-address]').val(),
	    		city = $('input[name=pi-g-city]').val(),
	    		state = $('select[name=pi-g-state] option:selected').val(),
	    		zip = $('input[name=pi-g-zip]').val(),
	    		email = $('input[name=pi-g-email]').val(),
	    		home = $('input[name=pi-g-homephone]').val(),
	    		cell = $('input[name=pi-g-cellphone]').val(),
	    		work = $('input[name=pi-g-workphone]').val();	    		

	    	if(parent == 'mother'){
	    		$('input[name=pi-mother]').val(name);
		    	$('input[name=pi-m-address]').val(addr);
		    	$('input[name=pi-m-city]').val(city);
		    	$('select[name=pi-m-state]').val(state);
		    	$('input[name=pi-m-zip]').val(zip);
		    	$('input[name=pi-m-email]').val(email);
		    	$('input[name=pi-m-homephone]').val(home);
		    	$('input[name=pi-m-cellphone]').val(cell);
		    	$('input[name=pi-m-workphone]').val(work);	
		    	setTimeout(function(){	    	
		    		deferred.resolve();	
		    	}, 1200);		    			    	
	    	}
	    	if(parent == 'father'){
	    		$('input[name=pi-father]').val(name);
		    	$('input[name=pi-f-address]').val(addr);
		    	$('input[name=pi-f-city]').val(city);
		    	$('select[name=pi-f-state]').val(state);
		    	$('input[name=pi-f-zip]').val(zip);
		    	$('input[name=pi-f-email]').val(email);
		    	$('input[name=pi-f-homephone]').val(home);
		    	$('input[name=pi-f-cellphone]').val(cell);
		    	$('input[name=pi-f-workphone]').val(work);
		    	setTimeout(function(){	    
		    		deferred.resolve();
		    	}, 1200);	
	    	}	
	    	return deferred.promise();
	    }

	    //CLEAR SECTION OF FIELDS
	    function clearFields(parent){
	    	if(parent == 'mother'){
	    		$( '.pi-m-checkbox' ).find( 'input' ).prop( "checked", false );

	    		$('input[name=pi-mother]').val('');
		    	$('input[name=pi-m-address]').val('');
		    	$('input[name=pi-m-city]').val('');
		    	$('select[name=pi-m-state]').val('');
		    	$('input[name=pi-m-zip]').val('');
		    	$('input[name=pi-m-email]').val('');
		    	$('input[name=pi-m-homephone]').val('');
		    	$('input[name=pi-m-cellphone]').val('');
		    	$('input[name=pi-m-workphone]').val('');
	    	}
	    	if(parent == 'father'){
	    		$( '.pi-f-checkbox' ).find( 'input' ).prop( "checked", false );

		    	$('input[name=pi-father]').val('');
		    	$('input[name=pi-f-address]').val('');
		    	$('input[name=pi-f-city]').val('');
		    	$('select[name=pi-f-state]').val('');
		    	$('input[name=pi-f-zip]').val('');
		    	$('input[name=pi-f-email]').val('');
		    	$('input[name=pi-f-homephone]').val('');
		    	$('input[name=pi-f-cellphone]').val('');
		    	$('input[name=pi-f-workphone]').val('');
	    	}
	    }
	    


	});

})(jQuery, this);