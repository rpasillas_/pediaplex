
              <?php
                /*
                 * This is the default post format.
                 *
                 * So basically this is a regular post. if you don't want to use post formats,
                 * you can just copy ths stuff in here and replace the post format thing in
                 * single.php.
                 *
                 * The other formats are SUPER basic so you can style them as you like.
                 *
                 * Again, If you want to remove post formats, just delete the post-formats
                 * folder and replace the function below with the contents of the "format.php" file.
                */
              ?>

              <article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">

                <div class="article-top">
                    <time class="updated entry-time" datetime="<?php echo get_the_time('Y-m-d'); ?>" itemprop="datePublished">
                        <span class="month"><?php echo get_the_time('M'); ?></span>
                        <span class="day"><?php echo get_the_time('d'); ?></span>
                        <span class="year"><?php echo get_the_time('Y'); ?></span>
                    </time>


                  <header class="article-header">

                    <h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>

                    <?php //check if megan kirkpatrick author  ?>
                    <?php if( get_the_author() == 'Megan Kirkpatrick' ){ ?> 
                         <div class="author"><?php the_author_posts_link(); ?></div>
                    <?php } ?>

                    <div class="category"><svg class="svg"><use xlink:href="#tagBlack"></use></svg><?php the_category( ', ' ); ?></div>
               
                  </header>
              </div>
              

                <section class="entry-content cf" itemprop="articleBody">
                  <?php
                    if ( has_post_thumbnail() ) {
                      the_post_thumbnail('bones-thumb-360');
                    }
                  ?>

                  <?php the_content();  ?>
                </section>

                <footer class="js-ssba-btn">
                  <button class="btn">Share This <i class="fa fa-arrow-right"></i></button> <?php echo do_shortcode('[ssba]'); ?>
                </footer>

                <?php comments_template(); ?>

              </article> <?php // end article ?>
