            <article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

              <div class="article-top">
                    <time class="updated entry-time" datetime="<?php echo get_the_time('Y-m-d'); ?>" itemprop="datePublished">
                        <span class="month"><?php echo get_the_time('M'); ?></span>
                        <span class="day"><?php echo get_the_time('d'); ?></span>
                        <span class="year"><?php echo get_the_time('Y'); ?></span>
                    </time>


                  <header class="article-header">

                    <h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>

                    <div class="category"><svg class="svg"><use xlink:href="#tagBlack"></use></svg><?php the_category( ', ' ); ?></div>
               
                  </header>
              </div>


                <section class="entry-content cf" itemprop="articleBody">
                  <?php
                      if ( has_post_thumbnail() ) {
                        the_post_thumbnail('bones-thumb-360');
                      }
                    ?>
                  <?php the_content(); ?>
                </section>

                <footer class="js-ssba-btn">
                  <button class="btn">Share This <i class="fa fa-arrow-right"></i></button> <?php echo do_shortcode('[ssba]'); ?>
                </footer>

                <?php comments_template('',true); ?>

              </article>
