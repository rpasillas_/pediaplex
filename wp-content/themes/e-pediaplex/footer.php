			<?php if( !is_front_page() ) get_template_part('partials/part', 'candybar'); ?>

			<?php if( !is_front_page() ) get_template_part('partials/part', 'recent-posts'); ?>


			<?php if( is_page(13)): //if the contact page ?>
				
				<?php get_template_part('partials/part', 'insurance'); ?>
				<?php get_template_part('partials/part', 'gallery'); ?>
			<?php else: ?>

				<?php get_template_part('partials/part', 'gallery'); ?>
				<?php get_template_part('partials/part', 'insurance'); ?>
				<?php get_template_part('partials/part', 'contact'); ?>
			<?php endif; ?>


			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<div class="footer-top">
					<div class="inner-container">
						<a href="<?php echo home_url(); ?>" class="footer-logo"><svg class="svg"><use xlink:href="#logoAltWhite" /></svg>Diagnostic &amp; Developmental Solutions For Your Child</a>

						<div class="footer-social">
							<span class="social-footer-heading">Connect With Us</span>
							<?php echo social_nav_alt(); ?>
						</div>

					</div>
				</div>
				<div class="footer-bottom">
					<div class="inner-container">
						<p class="source-org copyright"><?php bloginfo( 'name' ); ?>, LLC. | <?php echo do_shortcode('[address_single]');?> | <?php echo do_shortcode('[phone]'); ?> | &copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>.</p>

					</div>
				</div>				

			</footer>

		</div><?php //end #container ?>

		<a href="#abaForm"  id="formTrigger" class="fancybox" style="display:none;">Load Form</a>
		<div id="abaForm" style="display:none;">
			<span class="h1">Not sure where to begin?</span>
			<svg class="svg-ribbon ribbon-left"><use xlink:href="#left-banner-pink"></use></svg>
			<svg class="svg-ribbon ribbon-right"><use xlink:href="#right-banner-pink"></use></svg>
			<div class="popup-inner">
				<p><a href="<?php echo get_permalink(7); ?>">ABA Therapy</a> is a good place to start. Contact us today to learn more about how this therapy can help your child meet their maximum potential.</p>
				<hr>
				<p>Complete our patient intake form to start your child's journey today.</p>
				<a href="<?php echo get_permalink(33); ?>" class="btn"><svg class="svg-icon"><use xlink:href="#icon-form"></use></svg> Get Started</a>
			</div>
		</div>

		
		<script>
			//global variables for other scripts to consume
			windowWidth = window.innerWidth,
			windowHeight = window.innerHeight,
			mobileWidth = 617,
			resizeTimer = 0;

			function windowResize(){
				windowWidth = window.innerWidth,
				windowHeight = window.innerHeight;
			}

			//update the globals
			jQuery(window).on('resize', function($){
				clearTimeout(resizeTimer);
				resizeTimer = setTimeout( windowResize, 250 );			
			});
		</script>

		<?php wp_footer(); ?>	

	</body>

</html> <!-- end of site. what a ride! -->
