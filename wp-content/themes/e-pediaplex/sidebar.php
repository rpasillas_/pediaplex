				<aside id="sidebar" class="sidebar" role="complementary">

					<?php if (  is_active_sidebar( 'page-sidebar' ) && is_page()  ) : ?>

						<?php dynamic_sidebar( 'page-sidebar' ); ?>
					
					<?php elseif( is_active_sidebar( 'blog-sidebar' ) && !is_page() ) : ?>

						<?php dynamic_sidebar( 'blog-sidebar' ); ?>

					<?php endif; ?>

						<svg class="svg-ribbon"><use xlink:href="#left-banner-pink" /></svg>
						<svg class="svg-ribbon"><use xlink:href="#right-banner-pink" /></svg>

				</aside>