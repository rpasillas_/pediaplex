<?php get_header(); ?>

			<div id="content" class="inner-container">

				<div id="inner-content" class="wrap cf">
						
						<?php get_template_part('partials/part', 'home-recentposts'); ?>

						<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
						

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
								
								<svg class="svg-icon"><use xlink:href="#flippy-banner" /></svg>

								<header class="article-header">
									<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
								</header> <?php // end article header ?>

								<section class="entry-content cf" itemprop="articleBody">
									<?php the_content(); ?>
								</section> <?php // end article section ?>
								

								<footer class="article-footer cf">

								</footer>

							</article>

							<?php endwhile; endif; ?>

						</main>					

				</div>

			</div>


			<?php get_template_part('partials/part', 'featured-services'); ?>
			<?php get_template_part('partials/part', 'testimonials'); ?>




<?php get_footer(); ?>