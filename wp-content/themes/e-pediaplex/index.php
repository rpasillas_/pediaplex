<?php get_header(); ?>

			<div id="content" class="inner-container">

				<div id="inner-content" class="wrap cf">

						<?php get_sidebar(); ?>


						<main id="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article">
								<div class="post-content">
									<header class="article-header">
										<h1 class="h2 entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>	
									</header>


									<time class="updated entry-time" datetime="<?php echo get_the_time('Y-m-d'); ?>" itemprop="datePublished"><?php echo get_the_time(get_option('date_format')); ?></time>

									<div class="category"><svg class="svg"><use xlink:href="#tag"></use></svg><?php the_category( ', ' ); ?></div>
								</div>

								<div class="post-thumbnail">
									<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php echo (has_post_thumbnail()) ? get_the_post_thumbnail($post->ID, 'bones-thumb-360') : '<svg class="default-blog"><use xlink:href="#icon-flippy-white"></use></svg>'; ?></a>
								</div>

								<svg class="blog-logo-accent flippy-icon"><use xlink:href="#icon-flippy-white"></use></svg>
								

							</article>

							<?php endwhile; ?>

									<?php bones_page_navi(); ?>

									<?php echo do_shortcode('[bottom]'); ?>

							<?php else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the index.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>


						</main>

				</div>

			</div>


<?php get_footer(); ?>
