<?php get_header(); ?>

	<div id="content" class="inner-container">

		<div id="inner-content" class="wrap cf">


				<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

					
					<div class="form-wrap">
						<div class="inner-wrap">
							<span class="form-title">Drop Us a Line</span>
							<svg class="svg-ribbon"><use xlink:href="#left-banner-pink" /></svg>
							<svg class="svg-ribbon"><use xlink:href="#right-banner-pink" /></svg>

							<div class="contact-msg">
								<p>We'd love to hear from you!<br>Call us today or complete our <strong>patient intake<br>form</strong> to start your child's journey.</p>
								<a href="<?php echo get_permalink(33); ?>" class="btn"><svg class="svg-icon"><use xlink:href="#icon-form"></use></svg> Get Started</a>
							</div>

							<img src="<?php echo get_template_directory_uri(); ?>/library/img/children-painting.jpg">
							<img src="<?php echo get_template_directory_uri(); ?>/library/img/child-drawing.jpg">
						</div>
					</div>





					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

						<header class="article-header">
							<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
						</header> <?php // end article header ?>

						<section class="entry-content cf" itemprop="articleBody">
							<?php
								if ( has_post_thumbnail() ) {
									the_post_thumbnail('bones-thumb-360');
								}
							?>
							<?php the_content(); ?>

						</section> <?php // end article section ?>

					</article>



					<?php endwhile; else : ?>

							<article id="post-not-found" class="hentry cf">
								<header class="article-header">
									<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
								</header>
								<section class="entry-content">
									<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
								</section>
								<footer class="article-footer">
										<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
								</footer>
							</article>

					<?php endif; ?>

				</main>					

		</div>

	</div>

	<div id="map-wrap"><div id="map"></div></div>

<?php get_footer(); ?>