<!doctype html>

<html <?php language_attributes(); ?> class="no-js">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#ff0000">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
        <meta name="theme-color" content="#008da9">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<meta name="p:domain_verify" content="166ae8462bf996b3b4ce9afd671a5b96"/>
		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
		<?php include_once('library/img/sprite-sheet.svg'); ?>

		<div id="container">


			<header id="header" itemscope itemtype="http://schema.org/WPHeader">

				<div class="top-header cf inner-container">
					<p id="logo" class="h1" itemscope itemtype="http://schema.org/Organization">
						<a href="<?php echo home_url(); ?>" rel="nofollow">
							<svg class="svg"><use xlink:href="#mainLogo" /></svg>
							Diagnostic &amp; Developmental Solutions For Your Child
						</a>
					</p>

					<div class="header-contact">
						<?php echo do_shortcode('[phone]'); ?>
						
						<a href="<?php echo get_permalink(33); ?>" class="btn">Patient Intake Form</a>

						<nav role="navigation">
							<ul>
								<li><a href="<?php echo get_permalink(13); ?>">Contact</a></li>
								<li><a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">Blog</a></li>
							</ul>
						</nav>
					</div>	
				</div>


				<div id="mobile-header">
				
					<a href="<?php echo home_url(); ?>" rel="nofollow"><svg class="svg"><use xlink:href="#logoAlt" /></svg></a>

					<button id="mobile-menu-trigger">
						<span class="menu-bar">Navigation</span>
					</button>
				</div>	


				<nav role="navigation" id="nav" class="inner-container" itemscope itemtype="http://schema.org/SiteNavigationElement">

					<svg class="svg"><use xlink:href="#left-banner-blue" /></svg>

					<div class="mobile-phone-cta">
						Call Us Today<br>
						<?php echo do_shortcode('[phone]'); ?>
					</div>

					<div class="inner">

						<?php wp_nav_menu(array(
										'container' => false,                           
										'container_class' => 'menu cf',                 
										'menu' => 'The Main Menu', 
										'menu_class' => 'nav top-nav cf', 
										'theme_location' => 'main-nav', 
										'before' => '', 
										'after' => '',
										'link_before' => '',
										'link_after' => '',
										'depth' => 0,
										'fallback_cb' => '',
										'walker'	=> new nav_walker_menu
						)); ?>

						<?php echo social_nav(); ?>

					</div>
					<svg class="svg"><use xlink:href="#right-banner-blue" /></svg>

				</nav>

			</header>

			<?php //echo social_nav(); ?>

			
			<?php 

				/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				 * BRING IN EITHER THE SLIDER BANNER OR THE REGULAR BANNER
				 -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

				if( is_front_page() ) : 

					get_template_part('partials/banner', 'slider');
				else :

					get_template_part('partials/banner', 'static');
				endif;

			?>
