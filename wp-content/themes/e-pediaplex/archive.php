<?php get_header(); ?>

			<div id="content" class="inner-container">

				<div id="inner-content" class="wrap cf">

						<?php get_sidebar(); ?>

						
						<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (is_category()) { ?>
								<h1 class="archive-title h2">
									 <?php single_cat_title(); ?>
								</h1>

							<?php } elseif (is_tag()) { ?>
								<h1 class="archive-title h2">
									<span>Posts Tagged:</span> <?php single_tag_title(); ?>
								</h1>

							<?php } elseif (is_author()) {
								global $post;
								$author_id = $post->post_author;
							?>
								<h1 class="archive-title h2">

									<span>About</span> <?php the_author_meta('display_name', $author_id); ?>

								</h1>
							<?php } elseif (is_day()) { ?>
								<h1 class="archive-title h2">
									<span>Daily Archives:</span> <?php the_time('l, F j, Y'); ?>
								</h1>

							<?php } elseif (is_month()) { ?>
									<h1 class="archive-title h2">
										<span>Monthly Archives:</span> <?php the_time('F Y'); ?>
									</h1>

							<?php } elseif (is_year()) { ?>
									<h1 class="archive-title h2">
										<span>Yearly Archives:</span> <?php the_time('Y'); ?>
									</h1>
							<?php } ?>
							




							<?php 

								if( is_author(16) ){
									echo '<div class="bio">';
									echo get_avatar( get_the_author_meta('email'), '270' );
									echo '<p>' . get_the_author_meta('description') . '</p>'; 
									echo '</div>';
								}

								if( is_category('megs-story-matters') ){
									echo category_description();
									echo '<br>';
								}
							

							?>




							<section>

								<?php if( is_author(16) ){ ?>

									<h1><span>Posts By:</span> <?php the_author_meta('display_name', $author_id); ?></h1>
								<?php } ?>


							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article">
								<div class="post-content">
									<header class="article-header">
										<h1 class="h2 entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>	
									</header>


									<time class="updated entry-time" datetime="<?php echo get_the_time('Y-m-d'); ?>" itemprop="datePublished"><?php echo get_the_time(get_option('date_format')); ?></time>

									<div class="category"><svg class="svg"><use xlink:href="#tag"></use></svg><?php the_category( ', ' ); ?></div>
								</div>

								<div class="post-thumbnail">
									<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php echo (has_post_thumbnail()) ? get_the_post_thumbnail($post->ID, 'bones-thumb-360') : '<svg class="default-blog"><use xlink:href="#icon-flippy-white"></use></svg>'; ?></a>
								</div>

								<svg class="blog-logo-accent flippy-icon"><use xlink:href="#icon-flippy-white"></use></svg>
								

							</article>

							<?php endwhile; ?>
							
							</section>

									<?php bones_page_navi(); ?>

							<?php else : ?>

									<article id="post-not-found" class="hentry cf">
										<header class="article-header">
											<h1>Oops, Post Not Found!</h1>
										</header>
										<section class="entry-content">
											<p>Uh Oh. Something is missing. Try double checking things.</p>
										</section>
										<footer class="article-footer">
												<p>This is the error message in the archive.php template.</p>
										</footer>
									</article>

							<?php endif; ?>

						</main>

				</div>

			</div>

<?php get_footer(); ?>
